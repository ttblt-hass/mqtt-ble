"""Setup file."""
import sys

from setuptools import find_packages, setup

from mqtt_ble.__version__ import VERSION

if sys.version_info < (3, 7):
    sys.exit("Sorry, Python < 3.7 is not supported")

install_requires = list(val.strip() for val in open("requirements.txt"))
tests_require = list(val.strip() for val in open("test_requirements.txt"))

setup(
    name="mqtt_ble",
    version=VERSION,
    description=("Daemon which create gateway from ble to MQTT " "for Home-Assistant"),
    author="Thibault Cohen",
    author_email="titilambert@gmail.com",
    url="http://gitlab.com/titilambert/mqtt-light-decora",
    package_data={"": ["LICENSE.txt"]},
    include_package_data=True,
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "mqtt-ble = mqtt_ble.__main__:main",
        ]
    },
    license="Apache 2.0",
    install_requires=install_requires,
    tests_require=tests_require,
    classifiers=[
        "Programming Language :: Python :: 3.7",
    ],
)
